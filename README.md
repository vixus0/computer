# Computer

Stuff I need to have a working 'puter. I'm going to try using Guix because it has a lot of cool stuff for developers.

## Bootstrapping Guix

Due to my Thinkpad, I need to use the non-free kernel and firmware in the [nonguix channel]. Then, I build an install disk-image:

    guix system disk-image --file-system-type=iso9660 vixus/system/install.scm

This image gets burned to a USB stick. The disk image contains the files in `vixus/system/bootstrap` under `/etc/bootstrap` when I boot from the USB. I can use `partition.sh` to carve up my hard drive (with encryption) and then proceed with the installation as specified in [the manual][guix-manual-install].

[nonguix channel]: https://gitlab.com/nonguix/nonguix
[guix-manual-install]: https://guix.gnu.org/manual/en/html_node/Proceeding-with-the-Installation.html#Proceeding-with-the-Installation
