(define-module (vixus system config)
  #:use-module (gnu services)
  #:use-module (gnu system)
  #:use-module (gnu system install)
  #:use-module (nongnu packages linux)
  #:export (vixus-os))

(define bios-bootloader (bootloader-configuration
                          (bootloader grub-bootloader)
                          (target "/dev/sda1")))

(define efi-bootloader (bootloader-configuration
                         (bootloader grub-efi-bootloader)
                         (target "/boot/efi")))

(define vixus-os
  (operating-system
    (host-name "fox")
    (timezone "Europe/London")
    (locale "en_GB.utf8")

    ;; Choose between EFI and BIOS bootloader
    (bootloader (if (access? "/sys/firmware/efi" R_OK) efi-bootloader bios-bootloader))

    ;; Kernel
    (kernel linux)
    (firmware (cons* iwlwifi-firmware %base-firmware))

    ;; Filesystems
    (mapped-devices
     (list (mapped-device
            (source "/dev/sda2")
            (target "root")
            (type luks-device-mapping))))

    (file-systems (cons*
                    (file-system
                      (device (file-system-label "boot"))
                      (mount-point "/boot/efi")
                      (type "vfat"))
                    (file-system
                      (device (file-system-label "root"))
                      (mount-point "/")
                      (type "ext4")
                      (dependencies mapped-devices))
                    %base-file-systems))

    ;; Users
    (users (cons*
             (user-account
               (name "vixus")
               (group "users")
               (supplementary-groups '("wheel" "audio" "video"))
               (home-directory "/home/vixus"))
             %base-user-accounts))

    ;; Globally-installed packages
    (packages (cons*
                (list
                  (specification->package "nss-certs"))
                %base-packages))

    ;; Services
    ;; pcscd-service
    ;; libvirt-service
    (services (cons*
                (service dhcp-client-service-type)
                (service elogind-service-type)
                (service tlp-service-type)
                (service alsa-service-type)
                (service pcscd-service-type)
                %base-services))))

vixus-os
