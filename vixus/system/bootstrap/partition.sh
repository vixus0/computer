#!/bin/sh

# Make sure we're running bash
if test "x$BASH_VERSION" = x; then
  exec bash "$0" "$@"
fi

set \
  -o errexit \
  -o nounset \
  -o pipefail

abort() { echo "$@" >/dev/stderr; exit 1; }

test "$UID" -eq 0 || abort "This script must be run as root!"

usage="Usage: $0 device"
dev=${1:?$usage}

cat <<EOF
                     o     o
   _   __,   ,_  _|_   _|_     __   _  _
 |/ \_/  |  /  |  |  |  |  |  /  \_/ |/ |
 |__/ \_/|_/   |_/|_/|_/|_/|_/\__/   |  |_/
/|
\|         ~ wreck those drives ~

+++ $(date)
EOF

prt="parted -a optimal -s $dev"
rootpart="root"

# check if we're bios or efi
test -d /sys/firmware/efi && bootflag="esp" || bootflag="bios_grub"

echo "--- Creating new gpt partition table on $dev"
$prt \
  mklabel gpt \
  mkpart primary "0%" 550MiB \
  mkpart primary 550MiB "100%" \
  name 1 boot \
  name 2 crypt

echo "--- Setting boot partition flag: $bootflag"
$prt set 1 "$bootflag" on

echo "--- Formatting boot partition as FAT32"
mkfs.fat -n boot -F 32 "${dev}1"

echo "--- Encrypting root partition"
cryptsetup luksFormat "${dev}2"
echo -n "- uuid: "
cryptsetup luksUUID "${dev}2"
cryptsetup open --type luks "${dev}2" "$rootpart"

echo "--- Formatting root partition as ext4"
mkfs.ext4 -L "$rootpart" /dev/mapper/"$rootpart"

echo "--- Mounting root partition"
mount LABEL="$rootpart" /mnt

# swap size = RAM + 2GB
swapsize=$(free -m | awk '/Mem/ {print $2 + 2048}')
echo "--- Creating swapfile with size: $swapsize MiB"
dd if=/dev/zero of=/mnt/swapfile bs=1MiB count="$swapsize"
chmod a-rwx,u+rw /mnt/swapfile # only give root access
mkswap /mnt/swapfile
swapon /mnt/swapfile

bootpath="/mnt/boot"
test "$bootflag" = esp && bootpath="$bootpath/efi"
echo "--- Mounting boot partition to $bootpath"
mkdir -p "$bootpath"
mount LABEL=boot "$bootpath"
