(cons*
  (channel
    (name 'vixus)
    (url "https://gitlab.com/vixus0/computer"))
  (channel
    (name 'nonguix)
    (url "https://gitlab.com/nonguix/nonguix"))
  %default-channels)
