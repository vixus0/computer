(define-module (vixus system install)
  #:use-module (gnu services)
  #:use-module (gnu system)
  #:use-module (gnu system install)
  #:use-module (guix gexp)
  #:use-module (nongnu packages linux)
  #:export (installation-os-nonfree))

(define bootstrap-files
  (list
    `("bootstrap/config.scm" ,(local-file "bootstrap/config.scm"))
    `("bootstrap/channels.scm" ,(local-file "bootstrap/channels.scm"))
    `("bootstrap/partition.sh" ,(local-file "bootstrap/partition.sh"))))

(define bootstrap-files-service
  (simple-service 'bootstrap-files etc-service-type bootstrap-files))

(define installation-os-nonfree
  (operating-system
    (inherit installation-os)
    (kernel linux)
    (firmware (list linux-firmware))
    (services (cons*
                bootstrap-files-service
                (@@ (gnu system install) %installation-services)))))

installation-os-nonfree
